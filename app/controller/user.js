'use strict';

const Controller = require('egg').Controller;

class UserController extends Controller {
  async index() {
    console.log(this.ctx);
    this.ctx.body = 'hi, egg ma';
  }
  async delete() {
    //
    const ctx = this.ctx ;
    ctx.logger.info('user delete', ctx.request.body);
    await ctx.service.user.delById(ctx.request.body.id);
    ctx.body = "操作成功"
  }
  async create(){
    const ctx = this.ctx ;
    ctx.logger.info('user create', ctx.body);
    let user= await ctx.service.user.create(ctx.body);
    ctx.body = {data:user._id}
  }

  async profile(){
    const ctx = this.ctx ;
    ctx.logger.info('user delete', ctx.params);
    ctx.body =await ctx.service.user.findById(ctx.query.id);
  }

}

module.exports = UserController;
