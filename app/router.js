'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/user/profile', controller.user.profile);
  router.post('/user/del', controller.user.delete);
  router.post('/user/create', controller.user.create);
};
