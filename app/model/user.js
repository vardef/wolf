'use strict';

module.exports = app => {
  const { STRING, INTEGER, DATE } = app.mongoose;
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  //定义Schema
  const UserSchema = new Schema({
    userName: { type: String  },
    password: { type: String  },
    __v: { type: Number, select: false} //__ 查询时，不返回
  });
  return mongoose.model('User', UserSchema);
};
