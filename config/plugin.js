'use strict';

// had enabled by egg
// exports.static = true;
// 激活mongoose
exports.mongoose = {
  enable: true,
  package: 'egg-mongoose',
};
//是否开启静态目录，默认public目录
exports.static = true;
//开启验证插件,ctx.validate,对输入参数进行校验
exports.validate = {
  enable: true,
  package: 'egg-validate',
};

exports.cors = {
  enable: true,
  package: 'egg-cors',
};
