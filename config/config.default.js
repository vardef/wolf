'use strict';

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1541489464317_3090';

  // add your config here
  config.middleware = [];


  //mongoose
  config.mongoose = {
    client: {
      url: 'mongodb://127.0.0.1/wolf',
      options: {},
    }
  }




  // # 在config.{env}.js中配置，注意配置覆盖的问题
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true
    },
    domainWhiteList: ['*']
  };
  // 非安全设置 ，上线后，调整
  config.cors = {
    origin:'*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  };


  return config;
};
